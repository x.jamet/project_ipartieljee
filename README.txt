url pour acc�der au web service : localhost:8080/eclipselink/appPartiel/+ le nom du service 

le script de la base de donn�e : 

create sequence req_User;

create table userPartiel (
id  integer PRIMARY KEY DEFAULT nextval('req_User'),
nomUser varchar
);

insert into userPartiel(nomUser) values ('Kennedy');


create sequence req_Order;


create table orderPartiel(
id  integer PRIMARY KEY DEFAULT nextval('req_Order'),
prix float
);

insert into orderPartiel (prix) values(20.00);

create sequence req_product;

create table productPartiel(
id integer PRIMARY KEY DEFAULT nextval('req_product'),
nomProduct varchar
);

insert into productPartiel (nomProduct) values ('baguette magique');

CREATE TABLE productOrderPartiel (
 id_Order INTEGER NOT NULL,
 id_Product INTEGER NOT NULL,
 FOREIGN KEY (id_Product) REFERENCES productPartiel (id),
 FOREIGN KEY (id_Order) REFERENCES orderPartiel (id)
);

insert into productOrderPartiel (id_Order,id_Product) values (1,1);

ALTER TABLE orderPartiel add id_user integer;
ALTER TABLE orderPartiel ADD FOREIGN key (id_user) references userPartiel(id);



insert into orderPartiel (prix,id_user) values (30,3)