package com.ort.partielJee.PartieJee;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.ort.partielJee.PartielJee.entities.OrderPartiel;



public class StudentTest{

	@Before
	public void setUp() {
	    MockitoAnnotations.initMocks(this);
	    
	}
	
	@Test
	public void TestStudentName()
	{
		
		OrderPartiel order = Mockito.mock(OrderPartiel.class);
		
		Mockito.when(order.getPrix()).thenReturn(10.0);
		 
		System.out.println(order.getPrix()); // affiche "10.0"
		 
		order.setPrix(20.0);
		System.out.println(order.getPrix()); // affiche toujours "10.0"
		
		System.out.println(order.getPrix() + order.getPrix()); //affiche  "20.0"
		
		System.out.println(order.getPrix() + 50.0); // affiche  "60.0"
		
		Mockito.when(order.getPrix()).thenCallRealMethod();
		
		System.out.println(order.getPrix()); // affiche  "0.0"
		
		order.setPrix(20.0);
		System.out.println(order.getPrix()); // affiche toujours "0.0"
		
		
		Mockito.doCallRealMethod().when(order).setPrix(Mockito.anyDouble());
		
		order.setPrix(20.0);
		System.out.println(order.getPrix() + 50.0); // affiche "70.0"
		
		
	}
	

}
