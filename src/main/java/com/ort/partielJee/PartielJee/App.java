package com.ort.partielJee.PartielJee;
import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("appPartiel")
public class App extends ResourceConfig{
    public App() {
        packages("com.ort.partielJee.PartielJee");
    }
}