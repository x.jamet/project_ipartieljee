package com.ort.partielJee.PartielJee.service;

import com.ort.partielJee.PartielJee.interfaces.ScopedEntityManager;

import com.ort.partielJee.PartielJee.managers.PersistenceManager;

import com.ort.partielJee.PartielJee.entities.UserPartiel;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


/**
 * 
 * @author Xavier
 * cette classe permet d'avoir accés aux informations de la table utilisateurs, on peut donc voir toutes les commandes, en insérer, en supprimer ou même les modifier 
 * on peut les selectionner par identifiant  
 */

@Stateless
@LocalBean
@Path("/user")
public class UserService {

	/**
	 * utilisation de la méthode get en web service pour pouvoir afficher la liste de tous les utilisateurs
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserPartiel> getAllUser(){
		try(ScopedEntityManager em = getScopedEntityManager()){
			
			
    		List<UserPartiel> User = em.createQuery("select u from UserPartiel u").getResultList();
    		
			return User;
		}
	}
	
	/**
	 * utilisation de la méthode get en web service pour pouvoir afficher un utilisateur en particulier, on ajoute son identifiant dans l'url pour le choisir
	 * @param id
	 * @return
	 */
	@GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserPartiel getUser(@PathParam("id") Long id) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		TypedQuery<UserPartiel> query = em.createQuery("select s from UserPartiel s where s.id = :id", UserPartiel.class);
    		query.setParameter("id", id.intValue());
    		
    		UserPartiel user = query.getSingleResult();
    		
			return user;
		}
	}
	
	/**
	 * utilisation de la méthode delete en web service pour pouvoir supprimer un utilisateur en particulier, on ajoute son identifiant dans l'url pour le choisir
	 * @param id
	 */
	 @DELETE
	    @Path("{id}")
	    @Produces(MediaType.APPLICATION_JSON)
	    public void deleteStudent(@PathParam("id") int id) {
	    	try (ScopedEntityManager em = getScopedEntityManager()) {
	    		
	    		EntityTransaction tx = em.getTransaction();

	    		tx.begin();
	    		
	    		Query query = em.createQuery("delete from UserPartiel where id = :id");
	    		
	    		query.setParameter("id", id).executeUpdate();
	    		
	    		tx.commit();
	    		
	    	}
	    }
	/**
	 * utilisation de la méthode post en web service pour pouvoir ajouter un utilisateur avec un objet json
	 * @param user
	 */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void createUser(UserPartiel user) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		EntityTransaction tx = em.getTransaction();

    		tx.begin();

    		em.persist(user);
    		
    		tx.commit();
    		
    	}
    }
    /**
     * utilisation de la méthode put en web service pour pouvoir modifier les informations d'un utilisateur, choix gràce à son identifiant et un objet json pour sa modification
     * @param id
     * @param user
     */
    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void updateUser(@PathParam("id") int id, UserPartiel user) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		EntityTransaction tx = em.getTransaction();

    		tx.begin();
    		
    		
    		Query query = em.createQuery("update UserPartiel s set"
    				+ "  s.nomuser = :nomuser"
    				+ "  where s.id = :id");
    		
    		query.setParameter("id", id);
    		query.setParameter("nomuser", user.getNomuser());
    		
    		query.executeUpdate();
    		
    		tx.commit();
    		
    	}
    }
	
	private ScopedEntityManager getScopedEntityManager()
	{
		return PersistenceManager
				.getInstance()
				.getScopedEntityManagerFactory()
				.createScopedEntityManager();
	}
}
