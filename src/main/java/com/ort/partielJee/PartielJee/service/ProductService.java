package com.ort.partielJee.PartielJee.service;

import com.ort.partielJee.PartielJee.interfaces.ScopedEntityManager;

import com.ort.partielJee.PartielJee.managers.PersistenceManager;

import com.ort.partielJee.PartielJee.entities.ProductPartiel;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


/**
 * 
 * @author Xavier
 * cette classe permet d'avoir accés aux informations de la table produit, on peut donc voir toutes les commandes, en insérer, en supprimer ou même les modifier 
 * on peut les selectionner par identifiant  
 */

@Stateless
@LocalBean
@Path("/product")
public class ProductService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProductPartiel> getAllProduct(){
		try(ScopedEntityManager em = getScopedEntityManager()){
			
			
    		List<ProductPartiel> product = em.createQuery("select u from ProductPartiel u").getResultList();
    		
			return product;
		}
	}
	
	@GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ProductPartiel getProduct(@PathParam("id") Long id) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		TypedQuery<ProductPartiel> query = em.createQuery("select s from ProductPartiel s where s.id = :id", ProductPartiel.class);
    		query.setParameter("id", id.intValue());
    		
    		ProductPartiel product = query.getSingleResult();
    		
			return product;
		}
	}
	 @DELETE
	    @Path("{id}")
	    @Produces(MediaType.APPLICATION_JSON)
	    public void deleteProduct(@PathParam("id") int id) {
	    	try (ScopedEntityManager em = getScopedEntityManager()) {
	    		
	    		EntityTransaction tx = em.getTransaction();

	    		tx.begin();
	    		
	    		Query query = em.createQuery("delete from ProductPartiel where id = :id");
	    		
	    		query.setParameter("id", id).executeUpdate();
	    		
	    		tx.commit();
	    		
	    	}
	    }
	 
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void createProduct(ProductPartiel product) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		EntityTransaction tx = em.getTransaction();

    		tx.begin();

    		em.persist(product);
    		
    		tx.commit();
    		
    	}
    }
    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void updateProduct(@PathParam("id") int id, ProductPartiel product) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		EntityTransaction tx = em.getTransaction();

    		tx.begin();
    		
    		
    		Query query = em.createQuery("update ProductPartiel s set"
    				+ "  s.nomproduct = :nomproduct"
    				+ "  where s.id = :id");
    		
    		query.setParameter("id", id);
    		query.setParameter("nomproduct", product.getNomproduct());
    		
    		query.executeUpdate();
    		
    		tx.commit();
    		
    	}
    }
	
	private ScopedEntityManager getScopedEntityManager()
	{
		return PersistenceManager
				.getInstance()
				.getScopedEntityManagerFactory()
				.createScopedEntityManager();
	}
}
