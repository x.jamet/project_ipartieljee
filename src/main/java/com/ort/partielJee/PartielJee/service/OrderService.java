package com.ort.partielJee.PartielJee.service;

import com.ort.partielJee.PartielJee.interfaces.ScopedEntityManager;

import com.ort.partielJee.PartielJee.managers.PersistenceManager;

import com.ort.partielJee.PartielJee.entities.OrderPartiel;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


/**
 * 
 * @author Xavier
 * cette classe permet d'avoir accés aux informations de la table orderpartiel, on peut donc voir toutes les commandes, en insérer, en supprimer ou même les modifier 
 * on peut les selectionner par identifiant  
 */
@Stateless
@LocalBean
@Path("/order")
public class OrderService {
	
	/**
	 * utilisation de la méthode get en web service pour pouvoir afficher la liste de toutes les commandes
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<OrderPartiel> getAllOrder(){
		try(ScopedEntityManager em = getScopedEntityManager()){
			
			
			List<OrderPartiel> order = em.createQuery("select u from OrderPartiel u").getResultList();
    		
			return order;
		}
	}

	/**
	 * utilisation de la méthode get en web service pour pouvoir afficher une commande en particulier, on ajoute son identifiant dans l'url pour le choisir
	 * @param id
	 * @return
	 */
	@GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public OrderPartiel getOrder(@PathParam("id") Long id) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		TypedQuery<OrderPartiel> query = em.createQuery("select s from OrderPartiel s where s.id = :id", OrderPartiel.class);
    		query.setParameter("id", id.intValue());
    		
    		OrderPartiel order = query.getSingleResult();
    		
			return order;
		}
	}
	/**
	 * utilisation de la méthode delete en web service pour pouvoir supprimer une commande en particulier, on ajoute son identifiant dans l'url pour le choisir
	 * @param id
	 */
	 @DELETE
	    @Path("{id}")
	    @Produces(MediaType.APPLICATION_JSON)
	    public void deleteOrder(@PathParam("id") int id) {
	    	try (ScopedEntityManager em = getScopedEntityManager()) {
	    		
	    		EntityTransaction tx = em.getTransaction();

	    		tx.begin();
	    		
	    		Query query = em.createQuery("delete from OrderPartiel where id = :id");
	    		
	    		query.setParameter("id", id).executeUpdate();
	    		
	    		tx.commit();
	    		
	    	}
	    }
		/**
		 * utilisation de la méthode post en web service pour pouvoir ajouter une commande avec un objet json
		 * @param user
         */	 
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void createUser(OrderPartiel order) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		EntityTransaction tx = em.getTransaction();

    		tx.begin();

    		em.persist(order);
    		
    		tx.commit();
    		
    	}
    }
    /**
     * utilisation de la méthode put en web service pour pouvoir modifier les informations d'un utilisateur, choix gràce à son identifiant et un objet json pour sa modification
     * @param id
     * @param user
     */
    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void updateUser(@PathParam("id") int id, OrderPartiel order) {
    	try (ScopedEntityManager em = getScopedEntityManager()) {
    		
    		EntityTransaction tx = em.getTransaction();

    		tx.begin();
    		
    		
    		Query query = em.createQuery("update OrderPartiel s set"
    				+ "  s.prix = :prix"
    				+ "  where s.id = :id");
    		
    		query.setParameter("id", id);
    		query.setParameter("prix", order.getPrix());
    		
    		query.executeUpdate();
    		
    		tx.commit();
    		
    	}
    }
	
	private ScopedEntityManager getScopedEntityManager()
	{
		return PersistenceManager
				.getInstance()
				.getScopedEntityManagerFactory()
				.createScopedEntityManager();
	}
}