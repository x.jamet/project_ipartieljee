package com.ort.partielJee.PartielJee.interfaces;

import javax.persistence.EntityManagerFactory;

public interface ScopedEntityManagerFactory extends EntityManagerFactory {
public ScopedEntityManager createScopedEntityManager();
}