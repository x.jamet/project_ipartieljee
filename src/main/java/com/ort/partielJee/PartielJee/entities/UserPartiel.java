package com.ort.partielJee.PartielJee.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.ort.partielJee.PartielJee.entities.OrderPartiel;

import lombok.Data;

/**
 * 
 * @author Xavier
 * Cette entité correspond à la ta table userpartiel
 * elle renvoie les informations contenu dans cette table comme son id et le nom de l'utilisateur 
 *
 */
@Entity
@Data
public class UserPartiel {
	

    
	@Id
	@Column(name="id", nullable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(name="nomuser", nullable=false)
    String nomuser;

    public UserPartiel(String nomuser) {
        this.nomuser = nomuser;
    }
    public UserPartiel() {
    	
    }
    
    /**
     * ce bout de code si dessus devait normalement permettre de faire la jointure entre la table des commandes et celle des utilisateurs 
     * la table orderpartiel contient pour chaque commande l'identifiant de l'utilisateur
     */
	@JsonManagedReference
	@OneToMany(mappedBy="user", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<OrderPartiel> order;
	
	public Set<OrderPartiel> getPoints() {
		return order;
	}

	public void setPoints(Set<OrderPartiel> order) {
		this.order = order;
	}
}