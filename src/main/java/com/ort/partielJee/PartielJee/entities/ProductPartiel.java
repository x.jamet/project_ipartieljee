package com.ort.partielJee.PartielJee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;


/**
 * 
 * @author Xavier
 * Cette entité correspond à la ta table orderpartiel
 * elle renvoie les informations contenu dans cette table comme son id et le nom du produit
 *
 */

@Entity
@Data
public class ProductPartiel {
	

    
	@Id
	@Column(name="id", nullable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(name="nomproduct", nullable=false)
    String nomproduct;

    public ProductPartiel(String nomproduct) {
        this.nomproduct = nomproduct;
    }
    public ProductPartiel() {
    	
    }
}