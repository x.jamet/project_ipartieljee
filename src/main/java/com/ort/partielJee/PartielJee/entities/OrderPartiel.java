package com.ort.partielJee.PartielJee.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

import com.ort.partielJee.PartielJee.entities.UserPartiel;

import lombok.Data;

/**
 * 
 * @author Xavier
 * Cette entité correspond à la ta table orderpartiel
 * elle renvoie les informations contenu dans cette table comme son id et le prix de la commande
 *
 */
@Entity
@Data
public class OrderPartiel {
	

    
	@Id
	@Column(name="id", nullable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(name="prix", nullable=false)
    double prix;
	
	@JsonBackReference
	@JoinColumn(name="id_user")
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	private UserPartiel user;

    public OrderPartiel(double prix) {
        this.prix = prix;
    }
    public OrderPartiel() {
    	
    }
    
    public UserPartiel getUser() {
		return user;
	}

	public void setUser(UserPartiel user) {
		this.user = user;
	}
}