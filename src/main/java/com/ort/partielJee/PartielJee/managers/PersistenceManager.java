package com.ort.partielJee.PartielJee.managers;

import java.lang.reflect.Proxy;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.ort.partielJee.PartielJee.handlers.EntityManagerFactoryHandler;
import com.ort.partielJee.PartielJee.interfaces.ScopedEntityManagerFactory;


public class PersistenceManager {
	private static final PersistenceManager singleton = new PersistenceManager();
	protected ScopedEntityManagerFactory semf;
	

	private PersistenceManager() {
		
	}
	
	public ScopedEntityManagerFactory getScopedEntityManagerFactory() {
		if(semf==null) {
			createScopedEntityManagerFactory();
		}
		return semf;
	}
	public void closeEntityManagerFactory() {
		if(semf != null)
		{
			semf.close();
			semf = null;
		}
	}
	private void createScopedEntityManagerFactory() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("partiel");
		semf = (ScopedEntityManagerFactory) Proxy.newProxyInstance(
				ScopedEntityManagerFactory.class.getClassLoader(),
				new Class[] {ScopedEntityManagerFactory.class},
				new EntityManagerFactoryHandler(emf)
				);
		
	}
	public static PersistenceManager getInstance() {
		// TODO Auto-generated method stub
		return singleton;
	}
}

